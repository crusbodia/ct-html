jQuery(document).ready(function($) {

    var windowWidth = $(document).width();

    $(window).resize(function () {
        windowWidth = $(document).width();
        if (753 > windowWidth) {
            $("#tabs").tabs("destroy");
        } else {
            $("#tabs").tabs({
                active: 1,
                hide: { effect: "fadeOut", duration: 200 },
                show: { effect: "fadeIn", duration: 300 }
            });
        }
    });

    var $blockScroller;
    $blockScroller = $(".block-scroller .owl-carousel").not('.front-block-scroller .owl-carousel');
    $blockScroller.owlCarousel({
        items:5,
        dots: false,
        nav:true,
        navText: [
            '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            1024:{
                items:5
            },
            768:{
                items:4
            },
            600:{
                items:3
            },
            400:{
                items:2
            },
            100:{
                items:1,
                onChanged: function () {
                    var currentItem = this._current;
                    $blockScroller.find('.carousel-block-' + currentItem).trigger('click');
                }
            }
        }
    });

    var $frontBlockScroller;
    $frontBlockScroller = $(".front-block-scroller .owl-carousel");
    $frontBlockScroller.owlCarousel({
        items: 4,
        dots: false,
        nav:true,
        navText: [
            '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            1200:{
                items:4
            },
            768:{
                items:3
            },
            480:{
                items:2
            },
            100:{
                items:1,
                onChanged: function () {
                    var currentItem = this._current;
                    $frontBlockScroller.find('.carousel-block-' + currentItem).trigger('click');
                }
            }
        }
    });

    $(".block-show_reviews .owl-carousel").owlCarousel({
        items: 1,
        dots: false,
        nav:true,
        // loop:true,
        navText: [
            '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'
        ]
    });

    $(".block-clients .owl-carousel").owlCarousel({
        // items:9,
        dots: true,
        nav:true,
        navText: [
            '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'
        ],
        responsive:{
            1900: {
                items: 9
            },
            1700: {
                items: 8
            },
            1500: {
                items: 7
            },
            1440:{
                items:6
            },
            1024: {
                items:5
            },
            768:{
                items:4
                // nav: false
            },
            540:{
                items:3
            },
            375:{
                items:2
            },
            100:{
                items:1
            }
        }
    });

    $('.block-show_events .owl-carousel').on('initialized.owl.carousel', function(e) {
        if (e.page.count < 2) {
            $('.block-show_events .owl-controls').addClass('disabled-controls');
        }
    }).owlCarousel({
        items: 2,
        dots: true,
        responsive:{
            768:{
                items:2
            },
            100:{
                items:1
            }
        }
    });

    $(".block-slider .owl-carousel-slider").owlCarousel({
        items:1,
        dots: false,
        mouseDrag: false,
        loop:false,
        autoplay:false,
        autoplayTimeout: 5000,
        autoplayHoverPause:true
    });

    $(".event-gallery .owl-carousel").owlCarousel({
        items:4,
        dots: false,
        center: true,
        loop:true,
        responsive:{
            1024:{
                items:4
            },
            768:{
                items:3
            },
            480:{
                items:2
            },
            100:{
                items:1,
                center: false
            }
        }
    });

    $('.default-block-scroller .owl-item').css({
        'height' : $('.owl-stage').height()
    });

    $('select').selectmenu();

    if (753 <= windowWidth) {
        $("#tabs").tabs({
            active: 1,
            hide: { effect: "fadeOut", duration: 200 },
            show: { effect: "fadeIn", duration: 300 }
        });
    }

    /**
     * Show scroller details on icons click
     */
    var scrollerDetails = {
        defaults : {
            block_scroller : ''
        },
        init: function () {
            this.defaults.block_scroller = $('.block-scroller');
            this.defaults.block_scroller.each(function (i, el) {
                scrollerDetails.showCurrentBlockDetails(0);
                scrollerDetails.clickBlockListener($(el));
            });
        },

        showCurrentBlockDetails: function (id) {
            $('.scroller-details-block').stop().fadeTo(300, 0).hide();
            $('.scroller-details-block-' + id).fadeTo(300, 1);

            $('.carousel-block').parent().removeClass('carousel-block-active');
            $('.carousel-block-' + id).parent().addClass('carousel-block-active');
        },

        clickBlockListener: function ($el) {
            $el.find('.carousel-block').on('click', function () {
                var clickedItemId = $(this).data('itemId');
                scrollerDetails.showCurrentBlockDetails(clickedItemId);
            });
        }
    };

    scrollerDetails.init();

    // Menu control
    var $mainMenuWrap = $('.main-menu-wrap');
    $('.menu-show').on('click', function () {
        $mainMenuWrap.show("slide", { direction: "right" }, 300);
    });
    $('.menu-hide').on('click', function () {
        $mainMenuWrap.hide("slide", { direction: "right" }, 300);
    });

    // Sub menu controll
    var $menuIcon = $('.menu-icon');
    var $currentMenuParent = $('.current-menu-parent');
    $currentMenuParent.find('.menu-icon .fa').removeClass('fa-caret-down').addClass('fa-caret-up');
    $currentMenuParent.find('.sub-menu').addClass('active-sub-menu');

    $menuIcon.on('click', function (e) {
        var $el = $(this);
        var $currentSubMenu = $el.parent().find('.sub-menu');
        var $subMenu = $('.sub-menu');

        if ($currentSubMenu.hasClass('active-sub-menu')) {
            $currentSubMenu.removeClass('active-sub-menu');
            $subMenu.slideUp();
            $el.find('.fa').addClass('fa-caret-down').removeClass('fa-caret-up');
        } else {
            $subMenu.removeClass('active-sub-menu');
            $currentSubMenu.addClass('active-sub-menu');
            $subMenu.slideUp();
            $('.fa-caret-up').removeClass('fa-caret-up').addClass('fa-caret-down');
            $el.find('.fa').removeClass('fa-caret-down').addClass('fa-caret-up');
            $currentSubMenu.slideDown();
        }
    });

    // Load more Events
    var currentPage = 0;
    var $loadMoreEventsBtn = $('.load-more-events');
    $loadMoreEventsBtn.on('click', function () {
        var inProcess = false;
        currentPage = $(this).data('currentPage');
        currentPage++;
        $(this).data('current-page', currentPage);

        $.ajax({
            url: params.ajaxurl,
            type: 'post',
            data: {
                action: 'load_events',
                page: currentPage
            },
            beforeSend: function() {
                if ($loadMoreEventsBtn.hasClass('no-events-btn')) {
                    $loadMoreEventsBtn.fadeOut(500);
                    return false;
                }
                if (inProcess) {
                    return false;
                }
                inProcess = true;
                $loadMoreEventsBtn.addClass('in-process');
            },
            success: function( result ) {
                if ('' == result) {
                    $loadMoreEventsBtn.text('Р‘РѕР»СЊС€Рµ РЅРµС‚ РјРµСЂРѕРїСЂРёСЏС‚РёР№').addClass('no-events-btn');
                } else {
                    $('.passed-events .events-list-row').append(result);
                }
            },
            complete : function () {
                inProcess = false;
                $loadMoreEventsBtn.removeClass('in-process');
            }
        });
    });

    // Show vacancy details
    $('.vacancy-more').on('click', function () {
        $(this).find('.fa').toggleClass('fa-long-arrow-down').toggleClass('fa-long-arrow-up');
        $(this).parent().find('.vacancy-content').slideToggle();
    });

    var filterClients = {
        $select: null,
        $industry: null,
        $product: null,
        filters: {
            industry: '',
            product: ''
        },

        init: function () {
            var thiz = this;

            this.$select = $('.block-clients_success_history select');
            this.$industry = $('.block-clients_success_history select.select-industry');
            this.$product = $('.block-clients_success_history select.select-product');

            this.$select.on( "selectmenuchange", function( event, ui ) {
                thiz.filters.industry = thiz.$industry.val();
                thiz.filters.product = thiz.$product.val();
                thiz.renderSelected();
            } );
        },

        renderSelected: function () {
            var className = '';
            className = (this.filters.industry != '') ? '.' + this.filters.industry : '';
            className += (this.filters.product != '') ? '.' + this.filters.product : '';
            if ('' == className) {
                $('.client-item-wrap').removeClass('disabled').addClass('active');
            } else {
                $('.client-item-wrap').not(className).removeClass('active').addClass('disabled');
                $(className).removeClass('disabled').addClass('active');
            }
        }
    };

    filterClients.init();

    if ($('.block-scrolling_tabs').length > 0) {
        scrollerTabs();
    }

    function scrollerTabs() {
        var headerHeight = $('#top-wrap').outerHeight() + $('#wpadminbar').outerHeight();
        var menu = $('.scrolling-menu.sticky');
        var offset = menu.offset();
        var topOffset = offset.top;
        var leftOffset = offset.left;
        var marginTop = menu.css('marginTop');
        var marginLeft = menu.css('marginLeft');
        var width = menu.width();
        var block = $('.block-scrolling_tabs');

        $(window).resize(function () {
            headerHeight = $('#top-wrap').outerHeight() + $('#wpadminbar').outerHeight()
            width = menu.width();
            menu.css({
                // top: 0,
                position: 'relative',
                width: '28%'
            });
        });

        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop >= block.offset().top + block.height() - menu.outerHeight() - headerHeight - 70 - $('.block-scrolling_tabs .info').outerHeight()) {
                menu.css({
                    top: block.height() - menu.outerHeight() - 70 - $('.block-scrolling_tabs .info').outerHeight(),
                    // top: topOffset - menu.outerHeight() + 70,
                    position: 'relative',
                    width: '28%'
                });
            } else if (scrollTop >= topOffset - headerHeight - 45) {
                menu.css({
                    position: 'fixed',
                    top: headerHeight + 45,
                    width: width
                });
            } else {
                menu.css({
                    top: 0,
                    position: 'relative',
                    width: '28%'
                });
            }
        });

        //scroll2id
        $("a[rel='m_PageScroll2id']").mPageScroll2id({
            offset: headerHeight + 20,
            forceSingleHighlight:true
        });
    }

    if ($(".page-content a[href='#register-form']").length > 0) {
        $(".page-content a[href='#register-form']").mPageScroll2id({
            offset: 100,
            forceSingleHighlight:true
        });
    }

    // Video
    $('body').on('click', '.btn-video', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var options;
        var $wrapperVideo;
        var videoId;
        $wrapperVideo = $('#wrapper-video');
        videoId = $wrapperVideo.data('videoId');
        options = {
            videoId: videoId,
            start: 0,
            mute: false,
            repeat: false
        };
        $wrapperVideo.show();
        $wrapperVideo.tubular(options);
    });
    $('.close-video .destroy-video').on('click', function () {
        $('#wrapper-video').hide();
    });

    new WOW({
        offset: '100'
    }).init();

    $('#register-form').ajaxForm({
        crossDomain: true,
        dataType: 'jsonp',
        beforeSubmit: function (formData, jqForm, options) {
            var form = jqForm[0];
            var err = false;
            $(form).find('.error').removeClass('error');
            if (!form.first_name.value) {
                $(form.first_name).addClass('error');
                err = true;
            }
            if (!form.email.value || !validateEmail(form.email.value)) {
                $(form.email).addClass('error');
                err = true;
            }
            if (!form.phone.value) {
                $(form.phone).addClass('error');
                err = true;
            }
            if (!form.company.value) {
                $(form.company).addClass('error');
                err = true;
            }

            if (err)
                return false;
        },
        success: function (e) {
            $('#register-form').fadeOut();
            $('.form-success').fadeIn();
        },
        error: function (e) {
            if (e.status == 200) {
                $('#register-form').fadeOut();
                $('.form-success').fadeIn();
            } else {
                $('#register-form').fadeOut();
                $('.form-error').fadeIn();
            }
        }
    });

    $('#feedback').ajaxForm({
        crossDomain: true,
        dataType: 'jsonp',
        beforeSubmit: function (formData, jqForm, options) {
            var form = jqForm[0];
            var err = false;
            $(form).find('.error').removeClass('error');
            if ($(form.first_name).hasClass('required') && !form.first_name.value) {
                $(form.first_name).addClass('error');
                err = true;
            }
            if ($(form.email).hasClass('required') && !form.email.value || !validateEmail(form.email.value)) {
                $(form.email).addClass('error');
                err = true;
            }
            if ($(form.phone).hasClass('required') && !form.phone.value) {
                $(form.phone).addClass('error');
                err = true;
            }
            if ($(form.company).hasClass('required') && !form.company.value) {
                $(form.company).addClass('error');
                err = true;
            }

            if (err)
                return false;
        },
        success: function (e) {
            $('#feedback').fadeOut();
            $('.form-success').fadeIn();
        },
        error: function (e) {
            if (e.status == 200) {
                $('#feedback').fadeOut();
                $('.form-success').fadeIn();
            } else {
                $('#feedback').fadeOut();
                $('.form-error').fadeIn();
            }
        }
    });

    function validateEmail(emailaddress) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(emailaddress);
    }
});